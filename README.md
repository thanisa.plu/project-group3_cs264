# Project Group3_CS264
    Web App นี้จัดทำขึ้นเพื่อเป็นส่วนหนึ่งของการเรียนรู้
    ฐานข้อมูล เป็นการจำลองฐานข้อมูลขึ้นมา เพื่อทดสอบระบบเพียงเท่านั้น ไม่มีส่วนเกี่ยวข้องกับบุคคลจริง

## วิธีรัน Server
    รันผ่าน Command prompt หรือ Terminal โดยใช้คำสั่ง **node server.js**
    เปิดหน้าเว็บผ่าน Browser โดยใช้ URL **localhost:5000** 

## การ Login
    สามารถดู Email และ Password ได้ผ่านไฟล์ **student email and pass.txt**

## การส่งคำร้องขอจดทะเบียนล่าช้า
    จะสามารถทำได้เฉพาะในรายวิชาที่มีอยู่ใน ไฟล์ **subject.json**

## Google Sites
    สามารถดูได้ที่ 
    Link: https://sites.google.com/view/cs264-100001-group3/%E0%B8%AB%E0%B8%99%E0%B8%B2%E0%B9%81%E0%B8%A3%E0%B8%81?authuser=0 
